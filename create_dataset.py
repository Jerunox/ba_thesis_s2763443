#!/usr/bin/env python3

#file: create_dataset.py
#18-10-2017

import sys
import pickle
import numpy as np
import random

import nltk
from nltk.corpus import stopwords # Porter et al
from nltk.tokenize import TweetTokenizer

from os import listdir
from os.path import isfile, join


"""This function reads in tweets from pickle files and stores it in a dictionary"""
def read_files(categories):
	data_list = []
	id_list = []
	target_list = []

	target = 0
	treshold = 0
	for category in categories:
		treshold += 99000
		pickles = [f for f in listdir('dataset/' + category) if isfile(join('dataset/' + category, f))]

		for pickle_name in pickles:
			if ".pickle" not in pickle_name:
				continue

			pickle_content = pickle.load(open("dataset/" + category + "/" + pickle_name,"rb"))
			for tweet_id in pickle_content:
				tweet_text = pickle_content[tweet_id]

				if len(data_list) != treshold:
					data_list.append(tweet_text)
					id_list.append(tweet_id)
					target_list.append(target)

		target+=1

	np.random.seed(42)
	np.random.shuffle(data_list)
	np.random.seed(42)
	np.random.shuffle(id_list)
	np.random.seed(42)
	np.random.shuffle(target_list)


	data_collection = {'data':data_list,'id':id_list, 'target_names':categories, 'target':target_list}
	return data_collection


""" This function removes a token from a tweet """
def remove_token(line_in, token):
	tokenizer = TweetTokenizer()
	tokenized_line = tokenizer.tokenize(line_in)
	temp_list = []
	
	for word in tokenized_line:
		if token not in word:
			temp_list.append(word)  

	line_out = ' '.join(temp_list)
	return line_out


def remove_stop_words(line_in):
	tokenizer = TweetTokenizer()
	tokenized_line = tokenizer.tokenize(line_in)
	temp_list = []

	set_of_stopwords = set(stopwords.words("dutch"))

	for word in tokenized_line:
		if word not in set_of_stopwords:
			temp_list.append(word)

	line_out = ' '.join(temp_list)
	return line_out 


""" This function cleans a tweet. It removes the sarcasm hashtags and is capable of removing usernames and hyperlinks. """
def clean_up_tweets(data_collection, lowered = True, remove_handles = True, remove_url = True, no_stop_words = True):
	clean_data = []
	for tweet in data_collection["data"]:
		if lowered == True:
			tweet = tweet.lower()

		if remove_handles == True:
			tweet = nltk.tokenize.casual.remove_handles(tweet)

		if remove_url == True:
			tweet = remove_token(tweet,"http")

		if no_stop_words == True:
			tweet = remove_stop_words(tweet)

		tweet = remove_token(tweet,"#sarcasm")
		clean_data.append(tweet)

	data_collection["data"] = clean_data
	return data_collection



def main(outfilename):
	# Category names. Names must me names of folders in dataset directory
	categories = ["background"]#,"sarcasm"]
	# Loading tweets from pickle files.
	data_collection = read_files(categories)
	# Cleaning the data. e.g. removing URL's, Usernames, etcetera.
	data_collection = clean_up_tweets(data_collection)

	outputfile = "dataset/"+str(outfilename)
	with open(outputfile,"wb") as f:
		pickle.dump(data_collection,f, protocol=2)
	exit()


if __name__ == "__main__":
	if len(sys.argv) != 2:
		print("Usage: provide a name for .pickle outfile")
		exit()
	else:
		outfilename = sys.argv[1]	
		main(outfilename)
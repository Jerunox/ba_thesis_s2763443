#!/usr/bin/env python3
#zcat tweets/20161101\:*.out.gz | tools/tweet2tab id user text 

#file: background_tweets_filter.py
#18-10-2017

import sys
import pickle
import time
import random

def main(argv):


	start_time = time.time()

	outputname = argv[1]
	outputfile = "dataset/background/"+outputname+".pickle"

	#number_of_tweets_needed = int(argv[2])
	#number_of_tweets_needed = 20000
	#tweets_collected = 0


	print("Constructing pickle {}".format(outputname),file=sys.stderr)
	background_dict = {}

	for tweet in sys.stdin:
		random_number = random.randint(0, 50)
		if random_number == 1:
			if len(background_dict) != 100000:
				[ident,user,text] = tweet.rstrip().split('\t')
				lowered_text = text.lower()
				if "#sarcasme" not in lowered_text and "#ironie" not in lowered_text and "#not" not in lowered_text and "#sarcasm" not in lowered_text and "#irony" not in lowered_text:
					background_dict[ident] = text

	with open(outputfile,"wb") as f:
		pickle.dump(background_dict,f, protocol=2)


	logfile = open("dataset/background_log.txt",'a')


	filestring = "Pickle file"+outputname+"\n"
	numberstring = "Number of background tweets in pickle:"+str(len(background_dict))+"\n"
	elapsedstring = str(time.time() - start_time)+"\n\n"

	logfile.write(filestring)
	logfile.write(numberstring)
	logfile.write(elapsedstring)
	logfile.close()	

	print("Picle {} done in {}".format(outputname,elapsedstring))

if __name__ == "__main__":
	main(sys.argv)

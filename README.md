This is the repository for my BA-Thesis


the file classification.py is the main program to run.
classification.py requires three arguments: path to .pickle dataset, model number and classifier number.

always use the balanced set for the first argument.
The second argument is an integer:
	0: model with only unigrams
	1: model with only word embedding-based features
	2: model with unigrams + word embedding-based features

The third argument is an integer:
	0: Logistic regression
	1: K-Nearest Neighbours
	2: Decision Tree
	3: Random Forest
	4: Linear SVC

Example:
python3 classification.py dataset/balanced_set.pickle 0 0

- Runtimes are much longer with models 1 and 2.
- To use skewed dataset to classify on, change skew = False to skew = True in line 178.
- To 10-fold cross validate and write the results to a .txt file. Change show_statistics to True in line 268.
----------------------------------------------------------

The file create_dataset.py is used for creating the dataset .pickle file from the individual .pickle files.
This file also does al pre-processing. Since the complete datasets are already provided in this repository, you do not need to use it.
Same goes for the files sarcastic_tweets_filter.py and background_tweets_filter.py. These were used to retrieve the tweets from the Twitter2 corpus.

----------------------------------------------------------
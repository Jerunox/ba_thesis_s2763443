#!/usr/bin/env python3
#zcat tweets/20161101\:*.out.gz | tools/tweet2tab id user text 

#file: sarcastic_tweets_filter.py
#18-10-2017

import sys
import pickle
import time

def main(argv):


	start_time = time.time()

	outputname = argv[1]
	outputfile = "dataset/sarcasm/"+outputname+".pickle"

	print("Constructing pickle {}".format(outputname),file=sys.stderr)
	sarcasm_dict = {}

	for tweet in sys.stdin:
		[ident,user,text] = tweet.rstrip().split('\t')

		if "#sarcasme" in text:
			#print(text)
			sarcasm_dict[ident] = text

	with open(outputfile,"wb") as f:
		pickle.dump(sarcasm_dict,f, protocol=2)

	#print("Done!",file=sys.stderr)
	#print("number of sarcastic tweets:",len(sarcasm_dict))

	logfile = open("dataset/sarcasm_log.txt",'a')


	filestring = "Pickle file"+outputname+"\n"
	numberstring = "Number of sarcastic tweets in pickle:"+str(len(sarcasm_dict))+"\n"
	elapsedstring = str(time.time() - start_time)+"\n\n"

	logfile.write(filestring)
	logfile.write(numberstring)
	logfile.write(elapsedstring)
	logfile.close()	

	print("Picle {} done in {}".format(outputname,elapsedstring))

if __name__ == "__main__":
	main(sys.argv)

#!/usr/bin/env python3

#file: classification.py
#18-10-2017


import sys
import pickle
import numpy as np
import random
import nltk
import gensim
import itertools
import statistics

from collections import Counter
from os import listdir
from os.path import isfile, join

from nltk.corpus import stopwords # Porter et al
from nltk.tokenize import TweetTokenizer
from nltk.tag.perceptron import PerceptronTagger # https://github.com/evanmiltenburg/Dutch-tagger

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction import DictVectorizer
from sklearn.pipeline import Pipeline
from sklearn.base import TransformerMixin
from sklearn.pipeline import FeatureUnion

from sklearn.metrics import accuracy_score
from sklearn.metrics import recall_score
from sklearn.metrics import precision_score
from sklearn.metrics import f1_score
from sklearn.metrics import confusion_matrix

from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.svm import LinearSVC


class embedding_feature(TransformerMixin):
	"""Extract word embedding feature from tweet"""

	def fit(self, x, y=None):
		return self

	def transform(self, X):
		# Loading NLTK's Tweet Tokenizer
		tokenizer = TweetTokenizer()

		# Loading pre-trained word2vec model
		model = gensim.models.KeyedVectors.load_word2vec_format("word2vec_corpus/COW/cow-320.txt", binary=False)

		# Loading Dutch POS-tagger
		tagger = PerceptronTagger(load=False)
		tagger.load('model.perc.dutch_tagger_small.pickle')

		# Initialising output list. For every tweet a dictionary will be appended to this list.
		out = []


		for line in X:
			difference_score = None
			highest_score = None
			lowest_score = None


			tokenized_line = tokenizer.tokenize(line)
			tagged_line = tagger.tag(tokenized_line)

			content_words = []
			for tagged_token in tagged_line:
				if tagged_token[1][:4] == 'noun':
					content_words.append(tagged_token[0])

			number_Of_Content_Words = len(set(tokenized_line))
			number_Of_Content_Words = len(set(tagged_line))
			if number_Of_Content_Words > 1:
				pair_list = []
	
				for pair in itertools.combinations(tokenized_line, 2):
					reversed_pair = pair[::-1]

					if pair[0] != pair[1] and pair not in pair_list and reversed_pair not in pair_list:
						pair_list.append(pair)

				score_list = []
				for pair in pair_list:
					if pair[0] in model.wv.vocab and pair[1] in model.wv.vocab:
						score_tuple = (pair,model.similarity(pair[0],pair[1]))
						score_list.append(score_tuple)

				if len(score_list) > 1:
					sorted_by_score = sorted(score_list, key=lambda tup: tup[1])
					highest_score = sorted_by_score[-1][1]
					lowest_score = sorted_by_score[0][1]
					difference_score = highest_score - lowest_score

			if difference_score == None:
				out.append({})	
			else:	
				# Selection of only using difference score, or add the min/max score
				out.append({'embedding_difference_score': difference_score})
				#out.append({'embedding_difference_score': difference_score, 'embedding_highest_score': highest_score, 'embedding_lowest_score': lowest_score})
		return out


""" This function splits all data in training and testing data """
def split_data(data_collection, split = 0.90, skew = True):
	print("split data..")
	split_point = int(split*len(data_collection["data"]))
	X_train, X_test = data_collection["data"][:split_point], data_collection["data"][split_point:]
	Y_train, Y_test = data_collection["target"][:split_point], data_collection["target"][split_point:]
	assert(len(X_train)==len(Y_train))
	assert(len(X_test)==len(Y_test))

	if skew == True:
		X_test, Y_test = skew_data(X_test, Y_test)

	print("#train instances: {} #test instances: {}".format(len(X_train),len(X_test)))
	return X_train, X_test, Y_train, Y_test


def get_majority_label(labels):
	majority_label = Counter(labels).most_common()[0][0]
	majority_prediction = [majority_label for label in labels]
	return majority_label, majority_prediction


def shuffle_data_collection(data_collection,seed):
	keys = ["data","id","target"]
	for key in keys:
		np.random.seed(seed)
		data = data_collection[key]
		np.random.shuffle(data)
		data_collection[key] = data
	return data_collection


def evaluation_scores(Y_test, Y_predicted):
	recall = recall_score(Y_test,Y_predicted)
	precision = precision_score(Y_test,Y_predicted)
	fscore = f1_score(Y_test,Y_predicted)
	return recall, precision, fscore


def skew_data(X_in, Y_in):
	pair_list = []
	for i in range(len(X_in)):
		pair_list.append((X_in[i],Y_in[i]))
	pair_list.sort(key=lambda pair: pair[1])
	sarcastic_set = pair_list[-1000:]
	X_test = []
	Y_test = []
	for pair in sarcastic_set:
		X_test.append(pair[0])
		Y_test.append(pair[1])
	
	background_collection = pickle.load(open("dataset/99kbackground.pickle","rb"))
	X_test = X_test + background_collection['data']
	Y_test = Y_test + background_collection['target']

	return X_test, Y_test


def main(dataset_Name,model_No,classifier_No):
	data_collection = pickle.load(open(dataset_Name,"rb"))	# Loading excisting dataset
	seed = random.randint(1, 10000)
	#seed = 42
	data_collection = shuffle_data_collection(data_collection,seed)
		


	# Splitting the data in testing and training data
	X_train, X_test, Y_train, Y_test = split_data(data_collection, skew = False) # If skey = True, training will be done on balanced set and testing on skewed set.
	
	# Retrieving majority label
	majority_label, majority_prediction = get_majority_label(Y_test)


	unigram_vectorizer = CountVectorizer()
	embedding_vectorizer = embedding_feature()

	classifier_list = [LogisticRegression(),KNeighborsClassifier(), DecisionTreeClassifier(),RandomForestClassifier(),LinearSVC()]
	

	# Model which only uses unigrams
	if model_No == 0:
		pipeline = Pipeline( [('vec', unigram_vectorizer),('clf', classifier_list[classifier_No])])

	# Model which only uses word embedding-based features
	elif model_No == 1:
		pipeline = Pipeline( [('embeddings', Pipeline([
			('selector', embedding_vectorizer),
			('embeddingFeats', DictVectorizer()), 
								])),
			('clf', classifier_list[classifier_No])])

	#model which uses both unigrams and word embedding-based features
	elif model_No ==2:
		pipeline = Pipeline([
			('features', FeatureUnion([
				('vec', unigram_vectorizer),
				('embeddings', Pipeline([
					('selector', embedding_vectorizer),
					('embeddingFeats', DictVectorizer()), 
								])),
		])),
			('clf', classifier_list[classifier_No])])


	# Training the model
	print("Training model...")
	pipeline.fit(X_train, Y_train)


	# Testing the model
	print("Testing model...")
	Y_predicted = pipeline.predict(X_test)

	print(len(Y_predicted))

	recall, precision, fscore = evaluation_scores(Y_test, Y_predicted)
	accuracy = accuracy_score(Y_test, Y_predicted)
	accuracy_majority_baseline = accuracy_score(Y_test, majority_prediction)


	# Evaluating the model
	print("Evaluation:")
	print("Accuracy:", accuracy)
	print("Accuracy Majority Baseline:", accuracy_majority_baseline)
	print("\n")
	print("precision:",precision)
	print("recall:",recall)
	print("fscore:",fscore)
	print(confusion_matrix(Y_test, Y_predicted))

	tn, fp, fn, tp = confusion_matrix(Y_test,Y_predicted).ravel()
#	print(tn,fp,fn,tp)


	# stores missed sarcastic tweets in a list for further analysis
	#tweetlist = []
	#nn = 0
	#for label in Y_test:
	#	if label == 1:
	#		if Y_predicted[nn] == 0:
	#			tweetlist.append(X_test[nn])
	#	nn += 1
	#with open("list2.pickle","wb") as f:
	#	pickle.dump(tweetlist,f, protocol=2)

	
	return precision, recall, fscore, accuracy, accuracy_majority_baseline

if __name__ == "__main__":
	if len(sys.argv) == 4:
		dataset_Name = sys.argv[1]
		model_No = int(sys.argv[2])
		classifier_No = int(sys.argv[3])
	else:
		print("Usage: Program needs three arguments. See README")
		exit()

	show_statistics = False

	# If show_statistics is True, then 10-fold crossvalidation will be used and the results will be written in a .txt file.
	if show_statistics == False:
		main(dataset_Name,model_No,classifier_No)
		exit()

	total_precision = 0
	total_recall = 0
	total_fscore = 0
	total_accuracy = 0
	total_majority = 0

	precision_list  = []
	recall_list = []
	fscore_list = []
	accuracy_list = []
	majority_list = []



	for i in range(10):
		print(i)
		precision, recall, fscore, accuracy, accuracy_majority_baseline = main(dataset_Name,model_No,classifier_No)

		total_precision += precision
		total_recall += recall
		total_fscore += fscore
		total_accuracy += accuracy
		total_majority += accuracy_majority_baseline

		precision_list.append(precision)
		recall_list.append(recall)
		fscore_list.append(fscore)
		accuracy_list.append(accuracy)
		majority_list.append(accuracy_majority_baseline)

	average_precision = total_precision/10
	average_recall = total_recall/10
	average_fscore = total_fscore/10
	average_accuracy = total_accuracy/10
	average_accuracy_majority = total_majority/10

	pstdev_precision = statistics.pstdev(precision_list)
	pstdev_recall = statistics.pstdev(recall_list)
	pstdev_fscore = statistics.pstdev(fscore_list)
	pstdev_accuracy = statistics.pstdev(accuracy_list)
	pstdev_majority = statistics.pstdev(majority_list)

	logfile = open("classification_log.txt",'a')

	logfile.write("Dataset: "+str(dataset_Name)+"\n")
	logfile.write("Model_No: "+str(model_No)+"\n")
	logfile.write("classifier_No: "+str(classifier_No)+"\n")
	logfile.write("precision list:"+str(precision_list)+"\n")
	logfile.write("recall list:"+str(recall_list)+"\n")
	logfile.write("fscore list:"+str(fscore_list)+"\n")
	logfile.write("accuracy list:"+str(accuracy_list)+"\n")
	logfile.write("majority list list:"+str(majority_list)+"\n")
	logfile.write("10 time average precision:"+str(average_precision)+"\n")
	logfile.write("10 time average recall:"+str(average_recall)+"\n")
	logfile.write("10 time average F1 score:"+str(average_fscore)+"\n")
	logfile.write("10 time average accuracy:"+str(average_accuracy)+"\n")
	logfile.write("10 time average majority baseline:"+str(average_accuracy_majority)+"\n")
	logfile.write("pstdev_precision:"+str(pstdev_precision)+"\n")
	logfile.write("pstdev_recall:"+str(pstdev_recall)+"\n")
	logfile.write("pstdev_fscore:"+str(pstdev_fscore)+"\n")
	logfile.write("pstdev_accuracy:"+str(pstdev_accuracy)+"\n")
	logfile.write("pstdev_majority:"+str(pstdev_majority)+"\n\n")

	logfile.close()